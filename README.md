# TestOpenTK
Приложение для рисования с использованием OpenGL и OpenTK в WPF.

# TestAvalon
Попытка сделать приложение с использованием AvalonDock.
https://github.com/xceedsoftware/wpftoolkit/wiki/AvalonDock

#NuGet

Установлены следующие пакеты:
 - https://www.nuget.org/packages/Extended.Wpf.Toolkit/
 - https://www.nuget.org/packages/OpenTK
 - https://www.nuget.org/packages/OpenTK.GLControl/

Install-Package Extended.Wpf.Toolkit -Version 3.3.0
Install-Package OpenTK -Version 2.0.0
Install-Package OpenTK.GLControl -Version 1.1.2349.61993



















