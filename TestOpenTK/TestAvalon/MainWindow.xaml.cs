﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon
{
    public partial class MainWindow : Window
    {
        OpenGL.IDrawSomeShit mDrawSomeShit;

        public MainWindow()
        {
            InitializeComponent();
            mDrawSomeShit = new OpenGL.DrawSomeShaders(Multisampling.Samples32);

            /*
            try
            {
                var xmlLayout = new Xceed.Wpf.AvalonDock.Layout.Serialization.XmlLayoutSerializer(mDockMan);
                xmlLayout.Deserialize("layout.xml");
            }
            catch (System.IO.FileNotFoundException e)
            {
                //  Set default.
            }
            */
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string s = "";
            using (var fs = new System.IO.StringWriter())
            {
                var xmlLayout = new Xceed.Wpf.AvalonDock.Layout.Serialization.XmlLayoutSerializer(mDockMan);
                xmlLayout.Serialize(fs);
                s = fs.ToString();
            }
            System.IO.File.WriteAllText("layout.xml", s, Encoding.UTF8);
        }

        private void mBorder1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var bth = mBorder1.BorderThickness;
            int w = (int)Math.Floor(mBorder1.ActualWidth - bth.Left - bth.Right);
            int h = (int)Math.Floor(mBorder1.ActualHeight - bth.Top - bth.Bottom);
            if (w < 1 || h < 1) return;

            mDrawSomeShit.RedrawImage(mImage1, w, h);
        }
    }
}

























