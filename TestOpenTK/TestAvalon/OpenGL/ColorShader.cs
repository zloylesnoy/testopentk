﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    class ColorShader : Shader
    {
        private const string mVertexCode = @"
uniform mat4 u_Matrix;

attribute vec4 a_Position;
attribute vec4 a_Color;

varying vec4 frag_Color;

void main(void) {
    frag_Color = a_Color;
    gl_Position = u_Matrix * a_Position;
}
";

        private const string mFragmentCode = @"
varying vec4 frag_Color;

void main(void) {
    gl_FragColor = frag_Color;
}
";

        public ColorShader() : base(new VertexX3C4()) { }

        protected override void GetUniformLocations()
        {
            mMatrixUniform = GL.GetUniformLocation(mProgramHandle, "u_Matrix");
        }

        public override bool Build()
        {
            return CompileCode(mVertexCode, mFragmentCode);
        }
    }
}


















