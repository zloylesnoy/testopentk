﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    class DrawSomeShaders : IDrawSomeShit
    {
        private Size mImageSize;
        private WriteableBitmap mBitmap;
        private OpenGLtoBitmap mDrawBuffer;
        private int mDrawCounter;

        private SimpleShader mShader;
        private OpenTK.Matrix4 mMatrix;
        private VertexArrayObject<VertexX3> mVAO;
        private int mSlice;


        public DrawSomeShaders(Multisampling aSamples)
        {
            mImageSize = new Size(0, 0);
            mBitmap = null;
            mDrawBuffer = new OpenGLtoBitmap(DepthSize.Disabled, aSamples);
            mDrawCounter = 0;
            mShader = new SimpleShader();
            mMatrix = OpenTK.Matrix4.CreateScale(1);
            mVAO = new VertexArrayObject<VertexX3>();
            mSlice = -1;
        }

        private void PrepareDraw()
        {
            mShader.Build();

            mSlice = mVAO.AddSlice(5);
            mVAO.Allocate();

            var vs = mVAO.Vertices();
            int n = 0;
            vs[n++] = new VertexX3(-1, 0, 0);
            vs[n++] = new VertexX3(0, 1, 0);
            vs[n++] = new VertexX3(1, 0, 0);
            vs[n++] = new VertexX3(0, -1, 0);
            vs[n++] = new VertexX3(-1, 0, 0);

            mVAO.Create(BufferUsageHint.StaticDraw);

            if (mDrawBuffer.Samples() != Multisampling.Disabled)
            {
                GL.Enable(EnableCap.Multisample);
            }

            GL.Disable(EnableCap.Lighting);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Disable(EnableCap.DepthTest);

            mShader.PrepareToDraw(ref mMatrix);
        }


        private void Draw()
        {
            if (mDrawCounter == 0) PrepareDraw();
            mDrawCounter++;

            GL.ClearColor(0.7f, 0.9f, 0.8f, 1.0f);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            mVAO.Bind();
            GL.LineWidth(5);
            mVAO.DrawSlice(mSlice, PrimitiveType.LineStrip);
        }

        public void RedrawImage(Image aImage, int aWidth, int aHeight)
        {
            mDrawBuffer.BeginDraw(aWidth, aHeight);
            Draw();
            mDrawBuffer.EndDraw(ref mBitmap);
            if (aImage.Source != mBitmap) aImage.Source = mBitmap;
        }
    }
}

























