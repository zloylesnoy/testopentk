﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    interface IDrawSomeShit
    {
        void RedrawImage(Image aImage, int aWidth, int aHeight);
    }

    class DrawSomeShit : IDrawSomeShit
    {
        private Size mImageSize;
        private WriteableBitmap mBitmap;
        private OpenGLtoBitmap mDrawBuffer;
        private int mDrawCounter;


        public DrawSomeShit(Multisampling aSamples)
        {
            mImageSize = new Size(0, 0);
            mBitmap = null;
            mDrawBuffer = new OpenGLtoBitmap(DepthSize.Disabled, aSamples);
            mDrawCounter = 0;
        }

        private void PrepareDraw()
        {
            if (mDrawBuffer.Samples() != Multisampling.Disabled)
            {
                GL.Enable(EnableCap.Multisample);
            }

            GL.Disable(EnableCap.Lighting);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Disable(EnableCap.DepthTest);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-100, 100, -100, 100, -100, 100);
        }


        private void Draw()
        {
            if (mDrawCounter == 0) PrepareDraw();
            mDrawCounter++;

            GL.ClearColor(0.7f, 0.9f, 0.8f, 1.0f);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.LineWidth(3.0f);
            GL.Color4(1f, 0f, 0f, 1f);

            GL.Begin(PrimitiveType.LineStrip);
            GL.Vertex2(100, 15);
            GL.Vertex2(mDrawCounter % 100 - 50, 100);
            GL.Vertex2(-100, 8);
            GL.Vertex2(17, -100);
            GL.Vertex2(100, 15);
            GL.End();
        }

        public void RedrawImage(Image aImage, int aWidth, int aHeight)
        {
            mDrawBuffer.BeginDraw(aWidth, aHeight, mDrawCounter % 5 == 0);
            Draw();
            mDrawBuffer.EndDraw(ref mBitmap);
            if (aImage.Source != mBitmap) aImage.Source = mBitmap;
        }
    }
}































