﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    internal struct FrameBufferObject
    {
        int mFBO;   // идентификатор FBO
        int mColor; // идентификатор буфера цвета
        int mDepth; // идентификатор Z-буфера

        public void Clear()
        {
            mFBO = -1;
            mColor = -1;
            mDepth = -1;
        }

        public void Bind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, mFBO);
        }

        public void Destroy()
        {
            if (mFBO > 0) GL.DeleteFramebuffer(mFBO);
            if (mColor > 0) GL.DeleteRenderbuffer(mColor);
            if (mDepth > 0) GL.DeleteRenderbuffer(mDepth);
            Clear();
        }

        private void CreateColorBuffer(int aWidth, int aHeight, int aSamples)
        {
            mColor = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, mColor);
            if (aSamples > 1)
            {
                GL.RenderbufferStorageMultisample(RenderbufferTarget.Renderbuffer,
                    aSamples, RenderbufferStorage.Rgba8, aWidth, aHeight);
            }
            else
            {
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer,
                    RenderbufferStorage.Rgba8, aWidth, aHeight);
            }
            GL.FramebufferRenderbuffer(
                FramebufferTarget.Framebuffer,
                FramebufferAttachment.ColorAttachment0,
                RenderbufferTarget.Renderbuffer,
                mColor);
        }

        private void CreateDepthBuffer(int aWidth, int aHeight, int aDepthBits, int aSamples)
        {
            RenderbufferStorage zSize = RenderbufferStorage.DepthComponent16;
            if (aDepthBits > 16) zSize = RenderbufferStorage.DepthComponent24;
            if (aDepthBits > 24) zSize = RenderbufferStorage.DepthComponent32;

            mDepth = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, mDepth);
            if (aSamples > 1)
            {
                GL.RenderbufferStorageMultisample(RenderbufferTarget.Renderbuffer,
                    aSamples, zSize, aWidth, aHeight);
            }
            else
            {
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer,
                    zSize, aWidth, aHeight);
            }
            GL.FramebufferRenderbuffer(
                FramebufferTarget.Framebuffer,
                FramebufferAttachment.DepthAttachment,
                RenderbufferTarget.Renderbuffer,
                mDepth);
        }

        public bool Create(int aWidth, int aHeight, int aDepthBits, int aSamples)
        {
            mFBO = GL.GenFramebuffer();
            Bind();
            CreateColorBuffer(aWidth, aHeight, aSamples);
            if (aDepthBits > 0) CreateDepthBuffer(aWidth, aHeight, aDepthBits, aSamples);

            //  Проверяем, что нет ошибок:
            FramebufferErrorCode error = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            return (error == FramebufferErrorCode.FramebufferComplete);
        }

        public void CopyTo(FrameBufferObject aTo, int aWidth, int aHeight)
        {
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, mFBO);
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, aTo.mFBO);
            GL.BlitFramebuffer(
                0, 0, aWidth, aHeight,
                0, 0, aWidth, aHeight,
                ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);
        }
    }
}
























