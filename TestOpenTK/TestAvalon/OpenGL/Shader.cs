﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    abstract class Shader
    {
        protected int mProgramHandle;
        protected int mMatrixUniform; // У нас все вершинные шейдеры используют матрицу.
        protected readonly IVertex mVertex;

        public Shader(IVertex aVertexFormat)
        {
            mProgramHandle = 0;
            mMatrixUniform = 0;
            mVertex = aVertexFormat;
        }

        protected abstract void GetUniformLocations();
        public abstract bool Build();

        /*
        public IVertexFormat VertexFormat()
        {
            return mVertexFormat;
        }
        */

        public virtual void PrepareToDraw(ref OpenTK.Matrix4 mMatrix)
        {
            GL.UseProgram(mProgramHandle);
            GL.UniformMatrix4(mMatrixUniform, false, ref mMatrix);
        }

        private int CompileShader(string aCode, ShaderType aType)
        {
            int shaderHandle = GL.CreateShader(aType);
            GL.ShaderSource(shaderHandle, aCode);
            GL.CompileShader(shaderHandle);

            int compileStatus = 0;
            GL.GetShader(shaderHandle, ShaderParameter.CompileStatus, out compileStatus);
            return (compileStatus == 0) ? 0 : shaderHandle;
        }

        protected bool CompileCode(string aVertexCode, string aFragmentCode)
        {
            int vertexShaderId = CompileShader(aVertexCode, ShaderType.VertexShader);
            int fragmentShaderId = CompileShader(aFragmentCode, ShaderType.FragmentShader);

            mProgramHandle = GL.CreateProgram();
            GL.AttachShader(mProgramHandle, vertexShaderId);
            GL.AttachShader(mProgramHandle, fragmentShaderId);

            mVertex.BindAttribLocation(mProgramHandle);
            GL.LinkProgram(mProgramHandle);

            GetUniformLocations(); // overriden

            int linkStatus = 0;
            GL.GetProgram(mProgramHandle, GetProgramParameterName.LinkStatus, out linkStatus);
            return linkStatus != 0;
        }

        protected bool CompileFiles(string mVertexFile, string mFragmentFile)
        {
            try
            {
                string vertexCode = System.IO.File.ReadAllText(mVertexFile, Encoding.UTF8);
                string fragmentCode = System.IO.File.ReadAllText(mFragmentFile, Encoding.UTF8);
                return CompileCode(vertexCode, fragmentCode);
            }
            catch
            {
                return false;
            }
        }
    }
}


















