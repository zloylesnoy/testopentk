﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    class SimpleShader : Shader
    {
        private const string mVertexCode = @"
uniform mat4 u_Matrix;

attribute vec4 a_Position;

void main(void) {
    gl_Position = u_Matrix * a_Position;
}
";

        private const string mFragmentCode = @"
void main(void) {
    gl_FragColor = vec4(1, 1, 1, 1);
}
";

        public SimpleShader() : base(new VertexX3()) { }

        protected override void GetUniformLocations()
        {
            mMatrixUniform = GL.GetUniformLocation(mProgramHandle, "u_Matrix");
        }

        public override bool Build()
        {
            return CompileCode(mVertexCode, mFragmentCode);
        }
    }
}


















