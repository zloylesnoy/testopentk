﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    class TexturedShader : Shader
    {
        private int mTextureUniform = 0;

        private const string mVertexCode = @"
uniform mat4 u_Matrix;

attribute vec4 a_Position;
attribute vec2 a_TexCoord;

varying vec2 frag_TexCoord;

void main(void) {
    frag_TexCoord = a_TexCoord;
    gl_Position = u_Matrix * a_Position;
}
";

        private const string mFragmentCode = @"
uniform sampler2D u_Texture;

varying vec2 frag_TexCoord;

void main(void) {
    gl_FragColor = texture2D(u_Texture, frag_TexCoord);
}
";

        public TexturedShader() : base(new VertexX3T2()) { }

        protected override void GetUniformLocations()
        {
            mMatrixUniform = GL.GetUniformLocation(mProgramHandle, "u_Matrix");
            mTextureUniform = GL.GetUniformLocation(mProgramHandle, "u_Texture");
        }

        public override bool Build()
        {
            return CompileCode(mVertexCode, mFragmentCode);
        }

        public override void PrepareToDraw(ref OpenTK.Matrix4 mMatrix)
        {
            base.PrepareToDraw(ref mMatrix);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.Uniform1(mTextureUniform, 1);
        }
    }
}























