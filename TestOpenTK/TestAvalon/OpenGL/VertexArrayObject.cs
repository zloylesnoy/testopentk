﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    //  Буфер разбивается на несколько последовательных участков.
    struct VaoSlice
    {
        public int mOffset; // Начало участка, в вершинах.
        public int mSize; // Размер участка, в вершинах.

        public VaoSlice(int aOffset, int aSize)
        {
            mOffset = aOffset;
            mSize = aSize;
        }

        public int End()
        {
            return mOffset + mSize;
        }
    }

    /*
    class VertexArrayObjectBase
    {
        private readonly IVertexFormat mVertexFormat; // Формат вершины
        private int mVaoId; // Идентификатор Vertex Array Object
        private int mVertexBufferId; // Идентификатор буфера вершин
        private List<VaoSlice> mSlices; // Участки буфера

        public VertexArrayObjectBase(IVertexFormat aVertexFormat)
        {
            mVertexFormat = aVertexFormat;
            mVaoId = 0;
            mVertexBufferId = 0;
            mSlices = new List<VaoSlice>();
        }

        public int VertexCount()
        {
            if (mSlices.Count == 0) return 0;
            return mSlices[mSlices.Count - 1].End();
        }
    }

    class VertexArrayObject<TVertex> : VertexArrayObjectBase
        where TVertex : IVertex
    {
        private TVertex[] mVertices; // Вершины в RAM

        public VertexArrayObject() : base(TVertex().Format())
        {
            mVertices = null;
        }
    }
    */


    class VertexArrayObject<TVertex>
        where TVertex : struct, IVertex
    {
        private readonly int mBytePerVertex;
        private int mVaoId; // Идентификатор Vertex Array Object
        private int mVertexBufferId; // Идентификатор буфера вершин
        private List<VaoSlice> mSlices; // Участки буфера
        private TVertex[] mVertices; // Вершины в RAM, должен быть хотя бы 1 элемент


        public VertexArrayObject()
        {
            mVaoId = 0;
            mVertexBufferId = 0;
            mSlices = new List<VaoSlice>();
            mVertices = new TVertex[1];
            mBytePerVertex = mVertices[0].SizeInBytes();
        }

        public int BytesPerVertex()
        {
            return mBytePerVertex;
        }

        public int VertexCount()
        {
            if (mSlices.Count == 0) return 0;
            return mSlices[mSlices.Count - 1].End();
        }

        //  Добавить участок из aVerticesInSlice вершин.
        //  Возвращает индекс нового участка.
        //  Вызывается до Allocate.
        public int AddSlice(int aVerticesInSlice)
        {
            int result = mSlices.Count;
            int begin = VertexCount();
            mSlices.Add(new VaoSlice(begin, aVerticesInSlice));
            return result;
        }

        private int FirstVertex(int aSlice)
        {
            return mSlices[aSlice].mOffset;
        }

        //  Выделить оперативную память для буфера вершин.
        public void Allocate()
        {
            mVertices = new TVertex[VertexCount()];
        }

        public TVertex[] Vertices()
        {
            return mVertices;
        }

        public void Bind()
        {
            GL.BindVertexArray(mVaoId);
        }

        //  Передаём вершины в видеопамять:
        public void Update()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, mVaoId);
            GL.BufferSubData(BufferTarget.ArrayBuffer, new IntPtr(0),
                VertexCount() * mBytePerVertex, mVertices);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public void DrawSlice(int aSlice, PrimitiveType aPrimitive)
        {
            GL.DrawArrays(aPrimitive, mSlices[aSlice].mOffset, mSlices[aSlice].mSize);
        }

        //  Создать буфер OpenGL и переслать вершины в видеопамять.
        public void Create(BufferUsageHint aUsage)
        {
            //  Создать идентификатор для Vertex Array Object:
            mVaoId = GL.GenVertexArray();
            //  Привязать Vertex Array Object:
            GL.BindVertexArray(mVaoId);

            //  Создать идентификатор для Vertex Buffer Object:
            mVertexBufferId = GL.GenBuffer();
            //  Привязать Vertex Buffer Object:
            GL.BindBuffer(BufferTarget.ArrayBuffer, mVertexBufferId);
            //  Загрузить вершины в Vertex Buffer Object:
            GL.BufferData(BufferTarget.ArrayBuffer,
                VertexCount() * mBytePerVertex, mVertices, aUsage);

            //  Задать формат вершин:
            mVertices[0].SetBufferFormat();

            //  Отменить все привязки буферов:
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }
    }
}























