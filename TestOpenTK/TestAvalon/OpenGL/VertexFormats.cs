﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using OpenTK.Graphics.OpenGL;

namespace TestAvalon.OpenGL
{
    public interface IVertex
    {
        int SizeInBytes();
        void BindAttribLocation(int aProgramHandle);
        void SetBufferFormat();
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VertexX3 : IVertex
    {
        public const int a_Position = 0;
        public OpenTK.Vector3 mPosition;

        public int SizeInBytes()
        {
            return Marshal.SizeOf(new VertexX3());
        }

        public void BindAttribLocation(int aProgramHandle)
        {
            GL.BindAttribLocation(aProgramHandle, a_Position, "a_Position");
        }

        public void SetBufferFormat()
        {
            int stride = SizeInBytes();

            GL.EnableVertexAttribArray(a_Position);
            GL.VertexAttribPointer(a_Position, 3, VertexAttribPointerType.Float,
                false, stride, 0);
        }

        public VertexX3(float aX, float aY, float aZ)
        {
            mPosition = new OpenTK.Vector3(aX, aY, aZ);
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VertexX3T2 : IVertex
    {
        public const int a_Position = 0;
        public OpenTK.Vector3 mPosition;
        public const int a_TexCoord = 1;
        public OpenTK.Vector2 mTexCoord;

        public int SizeInBytes()
        {
            return Marshal.SizeOf(new VertexX3T2());
        }

        public void BindAttribLocation(int aProgramHandle)
        {
            GL.BindAttribLocation(aProgramHandle, a_Position, "a_Position");
            GL.BindAttribLocation(aProgramHandle, a_TexCoord, "a_TexCoord");
        }

        public void SetBufferFormat()
        {
            int stride = SizeInBytes();

            GL.EnableVertexAttribArray(a_Position);
            GL.VertexAttribPointer(a_Position, 3, VertexAttribPointerType.Float,
                false, stride, 0);

            GL.EnableVertexAttribArray(a_TexCoord);
            GL.VertexAttribPointer(a_TexCoord, 2, VertexAttribPointerType.Float,
                true, stride, OpenTK.Vector3.SizeInBytes);
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VertexX3C4 : IVertex
    {
        public const int a_Position = 0;
        public OpenTK.Vector3 mPosition;
        public const int a_Color = 1;
        public OpenTK.Vector4 mColor;


        public int SizeInBytes()
        {
            return Marshal.SizeOf(new VertexX3C4());
        }

        public void BindAttribLocation(int aProgramHandle)
        {
            GL.BindAttribLocation(aProgramHandle, a_Position, "a_Position");
            GL.BindAttribLocation(aProgramHandle, a_Color, "a_Color");
        }

        public void SetBufferFormat()
        {
            int stride = SizeInBytes();

            GL.EnableVertexAttribArray(a_Position);
            GL.VertexAttribPointer(a_Position, 3, VertexAttribPointerType.Float,
                false, stride, 0);

            GL.EnableVertexAttribArray(a_Color);
            GL.VertexAttribPointer(a_Color, 4, VertexAttribPointerType.Float,
                true, stride, OpenTK.Vector3.SizeInBytes);
        }
    }
}
























