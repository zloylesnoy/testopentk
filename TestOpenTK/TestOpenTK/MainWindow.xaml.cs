﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using OpenTK.Graphics.OpenGL;

namespace TestOpenTK
{
    public partial class MainWindow : Window
    {
        private Size mImageSize;
        private WriteableBitmap mBitmap;
        private readonly Multisampling mSamples;
        private OpenGLtoBitmap mDrawBuffer;


        public MainWindow()
        {
            InitializeComponent();

            mImageSize = new Size(0, 0);
            mBitmap = null;
            mSamples = Multisampling.Samples32;
            mDrawBuffer = new OpenGLtoBitmap(0, mSamples);
        }

        private void PrepareDraw()
        {
            if (mSamples > 0) GL.Enable(EnableCap.Multisample);

            GL.Disable(EnableCap.Lighting);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Disable(EnableCap.DepthTest);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-100, 100, -100, 100, -100, 100);
        }

        private int mDrawCounter = 0;
        private void Draw()
        {
            if (mDrawCounter == 0) PrepareDraw();
            mDrawCounter++;

            GL.ClearColor(0.7f, 0.9f, 0.8f, 1.0f);
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);

            GL.LineWidth(3.0f);
            GL.Color4(1f, 0f, 0f, 1f);

            GL.Begin(PrimitiveType.LineStrip);
            GL.Vertex2(100, 15);
            GL.Vertex2(mDrawCounter % 100 - 50, 100);
            GL.Vertex2(-100, 8);
            GL.Vertex2(17, -100);
            GL.Vertex2(100, 15);
            GL.End();
        }

        private void RedrawImage()
        {
            var bth = mBorder.BorderThickness;
            int w = (int)Math.Floor(mBorder.ActualWidth - bth.Left - bth.Right);
            int h = (int)Math.Floor(mBorder.ActualHeight - bth.Top - bth.Bottom);
            if (w < 1 || h < 1) return;

            mDrawBuffer.BeginDraw(w, h);
            Draw();
            mDrawBuffer.EndDraw(ref mBitmap);

            if (mImage.Source != mBitmap)
            {
                mImage.Source = mBitmap;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RedrawImage();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RedrawImage();
        }
    }
}
