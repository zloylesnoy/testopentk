﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

using OpenTK.Graphics.OpenGL;

namespace TestOpenTK
{
    //  Формат Z-буфера.
    enum DepthSize : int
    {
        Disabled = 0,
        Bits16 = 16,
        Bits24 = 24,
        Bits32 = 32
    }

    //  32 - лучшее качество, меньше скорость
    enum Multisampling : int
    {
        Disabled = 0,
        Samples4 = 4,
        Samples8 = 8,
        Samples16 = 16,
        Samples32 = 32
    }

    //  Класс для рисования во WriteableBitmap средствами OpenGL.
    //  Используется так:
    //   1) вызываем BeginDraw
    //   2) рисуем, при этом всё рисование происходит во внутренний буфер в видеопамяти.
    //   3) вызываем EndDraw и он копирует кадр из видеопамяти во WriteableBitmap.
    //
    //  Multisampling сделан по примеру:
    //      http://ake.in.th/2013/04/02/offscreening-and-multisampling-with-opengl/
    class OpenGLtoBitmap
    {
        private readonly DepthSize mDepthSize;
        private readonly Multisampling mSamples;
        private bool mUseMultisampling; // относится к последнему кадру

        //  Буфер в видеопамяти автоматически расширяется, если кадр в него не входит,
        //  и никогда не сужается. Поэтому размер кадра может быть меньше размера
        //  буфера в видеопамяти.
        private int mWidth;  // ширина последнего кадра, пикселей
        private int mHeight; // высота последнего кадра, пикселей
        private int mBigWidth;  // ширина буфера в видеопамяти, пикселей
        private int mBigHeight; // высота буфера в видеопамяти, пикселей

        private FrameBufferObject mSingleFBO; // простой FBO
        private FrameBufferObject mMultiFBO; // FBO с multisampling
        private int mMultiTexture; // идентификатор текстуры для multisampling

        private OpenTK.GLControl mControl;


        //  aDepthSize - формат Z-буфера
        //  aSamples   - число samples
        public OpenGLtoBitmap(DepthSize aDepthSize, Multisampling aSamples)
        {
            mDepthSize = aDepthSize;
            mUseMultisampling = false;

            mWidth = 0;
            mHeight = 0;
            mBigWidth = 0;
            mBigHeight = 0;

            mSingleFBO = new FrameBufferObject();
            mSingleFBO.Clear();

            mMultiFBO = new FrameBufferObject();
            mMultiFBO.Clear();
            mMultiTexture = -1;

            var mode = new OpenTK.Graphics.GraphicsMode(
                OpenTK.DisplayDevice.Default.BitsPerPixel, // color
                (int)mDepthSize, // depth
                0,  // stencil
                0,  // samples
                0,  // accum
                2,  // buffers
                false); // stereo
            mControl = new OpenTK.GLControl(mode);
            mControl.MakeCurrent();

            //  Проверяем, какой уровень multisampling поддерживает железо:
            int s = GL.GetInteger(GetPName.MaxSamples);
            if (s > (int)aSamples) s = (int)mSamples;

            if      (s >= 32) mSamples = Multisampling.Samples32;
            else if (s >= 16) mSamples = Multisampling.Samples16;
            else if (s >=  8) mSamples = Multisampling.Samples8;
            else if (s >=  4) mSamples = Multisampling.Samples4;
            else              mSamples = Multisampling.Disabled;
        }

        //  Если параметр aDisableMultisampling равен true, то следующий
        //  кадр не будет использовать multisampling.
        public void BeginDraw(int aWidth, int aHeight, bool aDisableMultisampling = false)
        {
            if (OpenTK.Graphics.GraphicsContext.CurrentContext != mControl.Context)
            {
                mControl.MakeCurrent();
            }
            mWidth = aWidth;
            mHeight = aHeight;
            mUseMultisampling = (mSamples != Multisampling.Disabled) && !aDisableMultisampling;

            if (mWidth > mBigWidth || mHeight > mBigHeight)
            {
                //  Требуется расширить буфер в видеопамяти.
                mControl.MakeCurrent();
                DestroyBuffers();
                CreateBuffers();
            }

            if (mUseMultisampling) mMultiFBO.Bind();
            else mSingleFBO.Bind();
            GL.Viewport(0, 0, mWidth, mHeight);
        }

        public void EndDraw(ref WriteableBitmap aResult)
        {
            if (aResult == null || aResult.Width != mWidth || aResult.Height != mHeight)
            {
                aResult = new WriteableBitmap(
                    mWidth, mHeight,
                    96, 96, // dpiX, dpiY
                    System.Windows.Media.PixelFormats.Pbgra32,
                    BitmapPalettes.WebPalette);
            }

            if (mUseMultisampling)
            {
                //  Копируем кадр из mMultiFBO в mSingleFBO:
                mMultiFBO.CopyTo(mSingleFBO, mWidth, mHeight);
                mSingleFBO.Bind();
            }

            //  Копируем кадр из mSingleFBO в aResult:
            aResult.Lock();
            GL.ReadPixels(0, 0, mWidth, mHeight, PixelFormat.Bgra, PixelType.UnsignedByte, aResult.BackBuffer);
            aResult.AddDirtyRect(new Int32Rect(0, 0, mWidth, mHeight));
            aResult.Unlock();
        }

        private void DestroyBuffers()
        {
            mSingleFBO.Destroy();
            mMultiFBO.Destroy();

            if (mMultiTexture > 0) GL.DeleteTexture(mMultiTexture);
            mMultiTexture = -1;
        }

        //  Создаёт буфера в видеопамяти, куда мы будем рисовать средствами OpenGL.
        private void CreateBuffers()
        {
            //  Рассчитываем размер видеобуфера. Он будет кратным 32:
            const int step = (1 << 5) - 1;
            int needWidth = (mWidth + step) & ~step;
            int needHeight = (mHeight + step) & ~step;
            if (mBigWidth < needWidth) mBigWidth = needWidth;
            if (mBigHeight < needHeight) mBigHeight = needHeight;

            //  Создаём FBO (frame buffer object):
            bool ok = mSingleFBO.Create(mBigWidth, mBigHeight, (int)mDepthSize, 0);
            if (!ok) throw new Exception();

            if (mSamples != Multisampling.Disabled)
            {
                mMultiTexture = GL.GenTexture();
                GL.BindTexture(TextureTarget.Texture2DMultisample, mMultiTexture);
                GL.TexImage2DMultisample(TextureTargetMultisample.Texture2DMultisample,
                    (int)mSamples, PixelInternalFormat.Rgba8, mBigWidth, mBigHeight, false);

                ok = mMultiFBO.Create(mBigWidth, mBigHeight, (int)mDepthSize, (int)mSamples);
                if (!ok) throw new Exception();

                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer,
                    FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D,
                    mMultiTexture, 0);
            }
        }
    }
}

























